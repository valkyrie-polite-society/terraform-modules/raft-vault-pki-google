# Vault TLS certificates

resource "tls_private_key" "vault" {
  count = var.cluster_size

  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "tls_cert_request" "vault" {
  count = var.cluster_size

  key_algorithm   = "RSA"
  private_key_pem = tls_private_key.vault[count.index].private_key_pem

  dns_names = [
    "localhost",
    trimsuffix(google_dns_record_set.vault.name, "."),
    "${var.cluster_name}-vault-${count.index}.c.${var.project}.internal",
  ]

  ip_addresses = [
    "127.0.0.1",
    google_compute_address.private[count.index].address,
    google_compute_address.public[count.index].address,
  ]

  subject {
    common_name  = "Vault Server"
    organization = "Valkyrie Polite Society"
  }
}

resource "tls_locally_signed_cert" "vault" {
  count = var.cluster_size

  cert_request_pem   = tls_cert_request.vault[count.index].cert_request_pem
  ca_key_algorithm   = "RSA"
  ca_private_key_pem = data.terraform_remote_state.ca.outputs.intermediate_ca_private_key_pem
  ca_cert_pem        = data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem

  validity_period_hours = 24 * 69 # Nice

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}

# Consul TLS certificates

resource "tls_private_key" "consul" {
  count = var.cluster_size

  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "tls_cert_request" "consul" {
  count = var.cluster_size

  key_algorithm   = "RSA"
  private_key_pem = tls_private_key.consul[count.index].private_key_pem

  subject {
    common_name  = "Consul Client"
    organization = "Valkyrie Polite Society"
  }

  ip_addresses = [
    "127.0.0.1",
    google_compute_address.public[count.index].address,
  ]

  dns_names = [
    "localhost",
    "client.${var.consul_datacenter}.consul",
  ]
}

resource "tls_locally_signed_cert" "consul" {
  count = var.cluster_size

  ca_key_algorithm   = "RSA"
  cert_request_pem   = tls_cert_request.consul[count.index].cert_request_pem
  ca_private_key_pem = data.terraform_remote_state.ca.outputs.intermediate_ca_private_key_pem
  ca_cert_pem        = data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem

  validity_period_hours = var.tls_certificate_ttl_hours

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth",
  ]
}
