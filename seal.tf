locals {
  kms_key_ring   = var.create_keyring ? google_kms_key_ring.vault[0] : data.google_kms_key_ring.vault[0]
  kms_crypto_key = var.create_crypto_key ? google_kms_crypto_key.unseal[0] : data.google_kms_crypto_key.unseal[0]
  seal_configuration = {
    gcpckms = templatefile("${path.module}/templates/vault/seal-gcpckms.hcl", {
      project    = local.kms_key_ring.project
      region     = local.kms_key_ring.location
      key_ring   = local.kms_key_ring.name
      crypto_key = local.kms_crypto_key.name
    })

    transit = var.transit_seal != null ? templatefile("${path.module}/templates/vault/seal-transit.hcl", {
      address    = var.transit_seal.address
      token      = var.transit_seal.token
      key_name   = var.transit_seal.key_name
      mount_path = var.transit_seal.mount_path
    }) : ""
  }
}
