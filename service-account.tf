resource "google_service_account" "vault" {
  account_id   = "${var.cluster_name}-vault"
  display_name = "Vault Service Account - ${var.cluster_name}"
}


resource "google_kms_crypto_key_iam_policy" "crypto_key" {
  crypto_key_id = local.kms_crypto_key.id
  policy_data   = data.google_iam_policy.unseal.policy_data
}

data "google_iam_policy" "unseal" {
  binding {
    role = "roles/cloudkms.cryptoKeyEncrypterDecrypter"

    members = [
      "serviceAccount:${google_service_account.vault.email}",
    ]
  }
}

# Auto join
resource "google_project_iam_member" "network_viewer" {
  project = var.project
  role    = "roles/compute.networkViewer"
  member  = "serviceAccount:${google_service_account.vault.email}"
}

# Google secrets engine - service account creation
resource "google_project_iam_member" "gcp_secrets_engine" {
  for_each = local.gcp_secrets_roles
  project  = var.project
  role     = each.key
  member   = "serviceAccount:${google_service_account.vault.email}"
}

locals {
  gcp_secrets_roles = toset([
    "roles/iam.serviceAccountAdmin",
    "roles/iam.serviceAccountKeyAdmin",
    "roles/resourcemanager.projectIamAdmin",
  ])
}
