output "endpoints" {
  value = formatlist("https://%s:8200", google_compute_address.public.*.address)
}

output "root_token" {
  value     = var.auto_initialize ? vaultutility_initialization.target[0].root_token : ""
  sensitive = true
}

output "network_tag" {
  value = local.network_tag
}
