resource "google_compute_instance_group" "vault" {
  name      = var.cluster_name
  zone      = var.zone
  network   = data.google_compute_network.default.self_link
  instances = google_compute_instance.vault.*.self_link
}
