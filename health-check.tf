resource "google_compute_region_health_check" "vault" {
  name   = "asgard-vault"
  region = var.region

  https_health_check {
    port         = 8200
    request_path = "/v1/sys/health?standbyok=true"
  }
}
