resource "google_kms_key_ring" "vault" {
  count = var.create_keyring ? 1 : 0

  name     = "${var.cluster_name}-unseal"
  location = "us"
}

data "google_kms_key_ring" "vault" {
  count = var.create_keyring ? 0 : 1

  name     = var.kms_key_ring.name
  location = var.kms_key_ring.location
}

resource "google_kms_crypto_key" "unseal" {
  count = var.create_crypto_key ? 1 : 0

  name            = var.kms_crypto_key_name
  key_ring        = var.create_keyring ? google_kms_key_ring.vault[0].self_link : data.google_kms_key_ring.vault[0].self_link
  rotation_period = "100000s"
}

data "google_kms_crypto_key" "unseal" {
  count    = var.create_crypto_key ? 0 : 1
  name     = var.kms_crypto_key_name
  key_ring = var.create_keyring ? google_kms_key_ring.vault[0].self_link : data.google_kms_key_ring.vault[0].self_link
}
