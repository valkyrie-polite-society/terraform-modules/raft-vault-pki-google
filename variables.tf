variable "network" {
  type = string
}

variable "cluster_size" {
  default = 3
}

variable "allowed_ip_cidrs" {
  type    = list(string)
  default = []
}

variable "ui_enabled" {
  default = true
}

variable "cluster_name" {
  type = string
}

variable "transit_seal" {
  type = object({
    address    = string,
    token      = string,
    mount_path = string,
    key_name   = string,
  })
  default = null
}

variable "auto_initialize" {
  type    = bool
  default = true
}

variable "snapshot_keep_days" {
  type    = number
  default = 90
}

variable "dns_managed_zone" {
  type = string
}

variable "zone" {
  type = string
}

variable "region" {
  type = string
}

variable "project" {
  type = string
}

variable "machine_type" {
  type = string
}

variable "certificate_authority_remote_state_prefix" {}
variable "certificate_authority_remote_state_bucket" {}

variable "ca_file" {
  type = string
}

variable "create_keyring" {
  type    = bool
  default = true
}

variable "kms_key_ring" {
  type    = object({ name = string, location = string })
  default = null
}

variable "create_crypto_key" {
  type    = bool
  default = true
}

variable "kms_crypto_key_name" {
  type    = string
  default = "unseal"
}

variable "consul_datacenter" {
  type = string
}

variable "tls_certificate_ttl_hours" {
  default = 24 * 365
}

variable "consul_auto_join_tag" {
  type = string
}
