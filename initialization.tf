resource "vaultutility_initialization" "target" {
  count = var.auto_initialize ? 1 : 0

  address      = "https://${google_compute_instance.vault[0].network_interface.0.access_config.0.nat_ip}:8200"
  ca_cert_file = pathexpand(var.ca_file)
}
