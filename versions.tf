terraform {
  required_version = ">= 1.0"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.78.0"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "~> 2.2.0"
    }

    vaultutility = {
      source  = "adriennecohea/vaultutility"
      version = ">= 0.0.3"
    }
  }
}
